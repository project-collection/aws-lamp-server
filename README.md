# LAMP Stack in AWS

## Summary

Terraform codes to create a LAMP stack with their own VPC and network components.

![Architecture](architecture.png)

comment: 

* separated db server in private subnet is not yet implemented
* instead of EC2, RDS MariaDB can be used to host MySQL


## How to run

* Make sure that you have a SSH key and mentioned in variables.tf.
* Change the AWS region in variables.tf to your preferred one.
* create key pair (lamp_root) using aws console ! (in the same region terraform stack deployed)

* Use the below commands to build, review and execute.

```bash
terraform init  
terraform get  
terraform plan  
terraform apply  
```

* run the script in scripts/install_lamp.sh on ec2 to install lamp

## Test

Test your web server. In a web browser, type the public DNS address (or the public IP address) of your instance. If there is no content in /var/www/html, you should see the Apache test page.


## Reference

* [How To Deploy A LAMP Stack In AWS Using Terraform](https://cloudaffaire.com/how-to-deploy-a-lamp-stack-in-aws-using-terraform/)
* [Install MySQL on Amazon Linux in AWS EC2](https://medium.com/@as.vignesh/install-mysql-on-amazon-linux-in-aws-ec2-e31a842f7ae9)
* [Installing MySQL in an EC2 instance](https://medium.com/@chamikakasun/installing-mysql-in-an-ec2-instance-55d6a3e19caf)
* [How to Install MySQL on CentOS 7](https://www.linode.com/docs/databases/mysql/how-to-install-mysql-on-centos-7/)
* [Tutorial: Install a LAMP web server on Amazon Linux 2](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2.html#prepare-lamp-server)
