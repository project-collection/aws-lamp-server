#! /bin/bash

# Reference: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2.html#prepare-lamp-server
sudo yum update -y
# Install the lamp-mariadb10.2-php7.2 and php7.2 Amazon Linux Extras repositories 
# to get the latest versions of the LAMP MariaDB and PHP packages for Amazon Linux 2
sudo amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2
# install the Apache web server, MariaDB, and PHP software packages
sudo yum install -y httpd mariadb-server
# view the current versions of package
sudo yum info httpd
sudo yum info mariadb-server
# start apache web server
sudo systemctl start httpd
# Use the systemctl command to configure the Apache web server to start at each system boot
sudo systemctl enable httpd
# verify httpd is on
sudo systemctl is-enabled httpd
